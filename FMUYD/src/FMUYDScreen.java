import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.JPanel;


@SuppressWarnings("serial")
public class FMUYDScreen extends JPanel implements KeyListener, ActionListener{

	private FMUYDGame game;
	private BufferedImage img = new BufferedImage(48, 48, BufferedImage.BITMASK);;;;;

	private static YComparator ycomp = new YComparator();
	private long previousTime = 0, frameCounter = 0;

	private boolean keyLeft, keyRight, keyUp, keyDown, keySpace,
	keyA, keyD, keyW, keyS, keyF;

	private int zombiecounter = 400;


	FMUYDScreen(){

	}


	public void paintComponent(Graphics g) {

		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		game.characters.sort(ycomp);

		for (FMUYDCharacter c:game.characters){
			img = c.getImg();
			int x = (int)c.getX();
			int y = (int)c.getY();
			switch (c.getStatus()) {
			case 1: g.drawImage(img, x, y, x+img.getWidth()*2, y+img.getHeight()*2, 0, 0, 16, 16, null);
			break;
			case 2: g.drawImage(img, x, y, x+img.getWidth()*2, y+img.getHeight()*2, 16, 0, 32, 16, null);
			break;
			case 3: g.drawImage(img, x, y, x+img.getWidth()*2, y+img.getHeight()*2, 32, 0, 48, 16, null);
			break;
			case 4: g.drawImage(img, x, y, x+img.getWidth()*2, y+img.getHeight()*2, 0, 16, 16, 32, null);
			break;
			case 5: g.drawImage(img, x, y, x+img.getWidth()*2, y+img.getHeight()*2, 16, 16, 32, 32, null);
			break;
			case 6: g.drawImage(img, x, y, x+img.getWidth()*2, y+img.getHeight()*2, 32, 16, 48, 32, null);
			break;
			case 7: g.drawImage(img, x, y, 48+x+img.getWidth()*2, y+img.getHeight()*2, 0, 32, 24, 48, null);
			break;
			case 8: g.drawImage(img, x-48, y, 48+x+img.getWidth()*2-48, y+img.getHeight()*2, 24, 32, 48, 48, null);
			break;


			}


		}

	}

	public void setGame(FMUYDGame game) {
		this.game = game;
	}


	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			keyLeft = true;
		}
		if (key == KeyEvent.VK_RIGHT) {
			keyRight = true;
		}
		if (key == KeyEvent.VK_UP) {
			keyUp = true;
		}
		if (key == KeyEvent.VK_DOWN) {
			keyDown = true;
		}
		if (key == KeyEvent.VK_SPACE) {
			keySpace = true;
		}


		if (key == KeyEvent.VK_A) {
			keyA = true;
		}
		if (key == KeyEvent.VK_D) {
			keyD = true;
		}
		if (key == KeyEvent.VK_W) {
			keyW = true;
		}
		if (key == KeyEvent.VK_S) {
			keyS = true;
		}
		if (key == KeyEvent.VK_F) {
			keyF = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();

		if (key == KeyEvent.VK_LEFT) {
			keyLeft = false;
		}
		if (key == KeyEvent.VK_RIGHT) {
			keyRight = false;
		}
		if (key == KeyEvent.VK_UP) {
			keyUp = false;
		}
		if (key == KeyEvent.VK_DOWN) {
			keyDown = false;
		}
		if (key == KeyEvent.VK_SPACE) {
			keySpace = false;
		}


		if (key == KeyEvent.VK_A) {
			keyA = false;
		}
		if (key == KeyEvent.VK_D) {
			keyD = false;
		}
		if (key == KeyEvent.VK_W) {
			keyW = false;
		}
		if (key == KeyEvent.VK_S) {
			keyS = false;
		}
		if (key == KeyEvent.VK_F) {
			keyF = false;
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}


	@Override
	public void actionPerformed(ActionEvent arg0) {
		for (Iterator<FMUYDCharacter> iterator = game.characters.iterator(); iterator.hasNext();) {
			FMUYDCharacter c = iterator.next();
			c.intelligence();
			if (c.getLifePower() <= 0) {
				iterator.remove();
			}
		}

		int i = 1;
		for (FMUYDCharacter c:game.mainch){
			if(i == 1) {c.act(keyUp, keyDown, keyRight, keyLeft, keySpace);}
			else if (i == 2) {c.act(keyW, keyS, keyD, keyA, keyF);}
			i++;
		}


		for (FMUYDCharacter c:game.characters){
			c.move();
		}
		if (zombiecounter >= 80) {
			((FMUYDGame) game).spawnZombie();
			zombiecounter = 0;
		} else {
			zombiecounter++;
		}
		repaint();
		frameCounter++;
		if (frameCounter >= 100) {
			long currentTime = new Date().getTime();
			long fps = frameCounter*1000/(currentTime - previousTime);
			frameCounter = 0;
			previousTime = currentTime;
			game.label.setText("FPS: "+fps);
		}
	}

}
