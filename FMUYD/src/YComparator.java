import java.util.Comparator;

public class YComparator implements Comparator<FMUYDCharacter> {

	@Override
	public int compare(FMUYDCharacter o1, FMUYDCharacter o2) {
		if(o1.getY() > o2.getY()) {
			return 1;
		}
		return -1;
	}

}
