import java.util.ArrayList;
import java.util.List;

import javax.swing.JLabel;

public class FMUYDGame {

	final List<FMUYDCharacter> characters = new ArrayList<>();
	final List<FMUYDCharacter> mainch = new ArrayList<>();
	private int monstercount = 0;
	JLabel label;

	FMUYDGame(){

		MainCharacter mainc = new MainCharacter("Warrior", 0, 0, this);
		characters.add(mainc);
		mainc.readImg("/GameWarriorSprite.png");
		mainch.add(mainc);
		
		MainCharacter playertwo = new MainCharacter("Mage", 300, 200, this);
		characters.add(playertwo);
		playertwo.readImg("/GameMageSprite.png");
		mainch.add(playertwo);

	}

	public void spawnZombie() {
		Monster zombie = new Monster("Zombie "+(monstercount+1), (int)(Math.round(Math.random()*1))*1600, (int)(Math.random()*800), this);
		characters.add(zombie);
		zombie.readImg("/GameZombieSprite.png");
		monstercount++;
	}

}