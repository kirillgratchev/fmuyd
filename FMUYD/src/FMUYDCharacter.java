import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public abstract class FMUYDCharacter {

	protected double xpos, ypos, xvel, yvel;
	protected int status;
	protected String name;
	BufferedImage image;
	protected FMUYDGame game;

	protected int animationCounter;
	protected boolean attackCounter;

	protected double speed;
	protected int health, lifepower, attack, score;

	double getX() {
		return xpos;
	}
	void setX(double i) {
		xpos = i;
	}

	double getY() {
		return ypos;
	}
	void setY(double i) {
		ypos = i;
	}


	int getStatus() {
		return status;
	}
	void setStatus(int i){
		status = i;
	}

	String getName() {
		return name;
	}
	void setName(String n) {
		name = n;
	}

	BufferedImage getImg() {
		return image;
	}
	void readImg(String n) {
		try {
			image = ImageIO.read(getClass().getResourceAsStream(n));
		} catch(IOException e){
			e.printStackTrace();
		}
	}

	int getLifePower() {
		return lifepower;
	}



	FMUYDCharacter(String name, int x, int y, double speed, FMUYDGame g, int health, int attack){
		this.xpos = x;
		this.ypos = y;
		this.speed = speed;
		this.status = 1;
		this.name = name;
		this.game = g;

		this.health = health;
		lifepower = this.health;
		this.attack = attack;

		System.out.println("Character "+name+" with "+health+" Lifepower and "+attack+" Attack Power created");
	}

	public void attack(boolean dir) {
		int dmgDealt = attack+(4 - (int)(Math.random()*8));
		for (FMUYDCharacter c:game.characters) {
			//left
			if (dir == true && c.xpos + 100 > xpos && c.xpos < xpos && c.ypos + 40 > ypos && c.ypos - 40 < ypos) {
				c.getDamaged(dir, dmgDealt, this);
			}
			//right
			if (dir == false && c.xpos - 100 < xpos && c.xpos > xpos && c.ypos - 40 < ypos && c.ypos + 40 > ypos) {
				c.getDamaged(dir, dmgDealt, this);
			}

		}
	}

	public void getDamaged(boolean dir, int atk, FMUYDCharacter attacker) {
		if (dir == true) {
			xvel += - 3;
		}
		if (dir == false) {
			xvel += 3;
		}
		lifepower -= atk;
		/*THIS NEEDS IMPROVEMENT*/
		System.out.println(name+" hit by "+attacker.getName()+" for "+atk+" damage and is now at "+lifepower+" Lifepower");
		if(lifepower <= 0) {
			attacker.score(1);
			die();
		}

	}


	public void score(int s) {
		score += s;
	}
	
	public void die() {
		System.out.println(name+" dies with a final score of "+score);
		game.mainch.remove(this);
	}

	public void move() {
		xpos += xvel;
		ypos += yvel;
		xvel = decreaseSpeed(xvel);
		yvel = decreaseSpeed(yvel);
	}

	public void act(boolean keyUp, boolean keyDown, boolean keyRight, boolean keyLeft, boolean keySpace) {
		if (keySpace == true) {
			if (status == 4 || status == 5 || status == 6 || status == 7) {
				setStatus(7);
				if (attackCounter == false) {
					attack(false);
					attackCounter = true;
				}
			} else {
				setStatus(8);
				if (attackCounter == false) {
					attack(true);
					attackCounter = true;
				}
			}
		} else {
			attackCounter = false;
			if (keyLeft == true) {

				setX(xpos - speed);
				if (xpos < 0) {
					xpos = 0;
				}
				if (animationCounter >= 10) {
					if(status == 2) {
						setStatus(3);
					} else {
						setStatus(2);
					}
					animationCounter = 0;
				}
			}
			if (keyRight == true) {

				setX(xpos + speed);
				if (xpos > 1484) {
					xpos = 1484;
				}
				if (animationCounter >= 10) {
					if(status == 5) {
						setStatus(6);
					} else {
						setStatus(5);
					}
					animationCounter = 0;
				}
			}
			if (keyUp == true) {

				setY(ypos - speed);
				if (ypos < 0) {
					ypos = 0;
				}
				if (animationCounter >= 10) {
					if (keyRight == false && keyLeft == false) {
						if(status == 2 || status == 1) {
							setStatus(3);
						} else if (status == 3){
							setStatus(2);
						} else if (status == 5 || status == 4){
							setStatus(6);
						} else if (status == 6){
							setStatus(5);
						}
					}
					animationCounter = 0;
				}
			}
			if (keyDown == true) {

				setY(ypos + speed);
				if (ypos > 700) {
					ypos = 700;
				}
				if (animationCounter >= 10) {
					if (keyRight == false && keyLeft == false) {
						if(status == 2 || status == 1) {
							setStatus(3);
						} else if (status == 3){
							setStatus(2);
						} else if (status == 5 || status == 4){
							setStatus(6);
						} else if (status == 6){
							setStatus(5);
						}
					}
					animationCounter = 0;
				}
			}
			if (keyLeft == false && keyRight == false && keyUp == false && keyDown == false) {
				if (status == 5 || status == 6 || status == 7) {
					setStatus(4);

				} else if (status != 4){
					setStatus(1);

				}

			}
			if (animationCounter >= 10) {
				animationCounter = 0;
			}
			animationCounter++;
		}
	}

	abstract public void intelligence();

	public double decreaseSpeed(double a) {
		if (a > 0.1) {
			a -= 0.1;
		} else if (a < -0.1) {
			a += 0.1;
		} else {
			a = 0;
		}
		return a;
	}
}
