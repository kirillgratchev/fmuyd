import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.Timer;

public class FMUYDInitializer {

	
	
	FMUYDWindow getWindow() {
		
		
		
		FMUYDWindow window = new FMUYDWindow();
		FMUYDScreen screen = new FMUYDScreen();
		FMUYDGame game = getGame();
		
		
		JLabel label = new JLabel("F.M.U.Y.D.");
		game.label = label;
		
		window.setBounds(50, 50, 1600, 800);
		window.setLayout(new BorderLayout());
		window.setVisible(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.addKeyListener(screen);
		
		
		
		screen.setBounds(0, 0, 1600, 900);
		screen.setVisible(true);
		screen.setGame(game);
		window.add(screen, BorderLayout.CENTER);
		
		window.add(label, BorderLayout.NORTH);
		
		Timer timer = new Timer(15, screen);
		timer.start();
		
		
		return window;
	}
	
	FMUYDGame getGame() {
		
		FMUYDGame game = new FMUYDGame();
		
		
		return game;
		
	}
}
