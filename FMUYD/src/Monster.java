
public class Monster extends FMUYDCharacter {

	private int braintimecounter;
	private double mX, mY;

	Monster(String name, int x, int y, FMUYDGame g) {
		super(name, x, y, 1.5+Math.random()*1, g, (int)(80+Math.random()*60), (int)(10+Math.random()*6));

	}

	@Override
	public void intelligence() {

		boolean keyUp = false, keyDown = false, keyRight = false, keyLeft = false, keySpace = false;
		if (braintimecounter <= 0) {
			locateMainCharacter();
			braintimecounter = (int)(Math.random()*60+10);
		}
		if(mX - 70 > xpos) {
			keyRight = true;
		}
		if(mX + 70 < xpos) {
			keyLeft = true;
		}
		if(mY - 30 > ypos) {
			keyDown = true;
		}
		if(mY + 30 < ypos) {
			keyUp = true;
		}
		if(mX + 60 > xpos && mX < xpos && mY + 20 > ypos && mY - 20 < ypos ||
				mX - 60 < xpos && mX > xpos && mY - 20 < ypos && mY + 20 > ypos) {
			keySpace = true;
		}
		act(keyUp, keyDown, keyRight, keyLeft, keySpace);
		braintimecounter--;


	}

	private void locateMainCharacter(){
		double oneX = xpos, oneY = ypos, twoX = 100000, twoY = 100000;
		int i = 1;
		for (FMUYDCharacter c:game.mainch) {
			if (i == 1) {
				oneX = c.getX();
				oneY = c.getY();
			} else if(i == 2) {
				twoX = c.getX();
				twoY = c.getY();
			}
			i++;
		}
		double countOne = 0, countTwo = 0;

		if (oneX > xpos) {countOne = countOne + (oneX - xpos);}
		else {countOne = countOne + (xpos - oneX);}
		if (oneY > ypos) {countOne = countOne + (oneY - ypos);}
		else {countOne = countOne + (ypos - oneY);}

		if (twoX > xpos) {countTwo = countTwo + (twoX - xpos);}
		else {countTwo = countTwo + (xpos - twoX);}
		if (twoY > ypos) {countTwo = countTwo + (twoY - ypos);}
		else {countTwo = countTwo + (ypos - twoY);}

		if (countOne <= countTwo) {
			mX = oneX - 50 + Math.random()*100;
			mY = oneY - 50 + Math.random()*100;
		}
		else {
			mX = twoX - 50 + Math.random()*100;
			mY = twoY - 50 + Math.random()*100;
		}
	}

}
